#!/bin/sh

# Assumes "build-vol" mounted to /vol in the container, and a
# scratch tmpfs mount at /build to use for build

cd /vol
cp Makefile test.cpp /build
cd /build
make -j4 && cp test /vol/prt-navy-srv || exit 1
