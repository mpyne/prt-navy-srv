#include <served/served.hpp>
#include <string>
#include <utility>
#include <algorithm>
#include <iostream>

static void handle_score(served::response &res, const served::request &req)
{
	const auto &stock_reply = served::response::stock_reply;
	using std::stol;
	using std::make_pair;
	using std::cerr;

	const auto ctype = req.query["cardio_type"];
	if(ctype.empty() || (ctype != "bike" && ctype != "elliptical" &&
		ctype != "run" && ctype != "swim" && ctype != "treadmill"))
	{
		cerr << "wrong type " << req.query["cardio_type"] << "\n";
		stock_reply(400, res);
		return;
	}

	const auto cardio_score = req.query["cardio_score"];
	const auto core_score = req.query["core_score"];
	const auto upper_body_score = req.query["upper_body_score"];
	if(cardio_score.empty() || core_score.empty() || upper_body_score.empty()) {
		cerr << "empty score\n";
		stock_reply(400, res);
		return;
	}

	const auto to_num = [](const std::string &str) {
		unsigned result = 0;
		bool error = false;
		try {
			result = (unsigned)stol(str);
		}
		catch(...) {
			error = true;
			cerr << "num conv error\n";
		}

		return make_pair(error, result);
	};

	auto cardio_score_pair = to_num(cardio_score);
	auto core_score_pair = to_num(core_score);
	auto upper_body_score_pair = to_num(upper_body_score);
	const auto pair_list = {cardio_score_pair, core_score_pair, upper_body_score_pair};

	// validate
	if(std::any_of(pair_list.begin(), pair_list.end(), [](const auto &i) {
		return i.first || i.second < 40u || i.second > 100u;
	})) {
		cerr << "num conv error or value out of range\n";
		stock_reply(400, res);
		return;
	}

	const auto result = (cardio_score_pair.second + core_score_pair.second + upper_body_score_pair.second) / 3;

	// create response;
	std::string response =
R"({
"score_cardio":66,
"score_core":85,
"score_upper_body":70,
"score_readable":"GOOD MEDIUM",
"score_overall":)" + std::to_string(result) + "\n}\n";

	res << response;
}

int main(int argc, char *argv[])
{
	served::multiplexer mux;

	mux.handle("/status")
		.get([](served::response &res, const served::request &req) {
			res << R"({"status":"You'll find this battlestation is FULLY OPERATIONAL."})";
			res.set_header("Server", "Served/20171209");
		});
	mux.handle("/prt/getScore")
		.get([](served::response &res, const served::request &req) {
			handle_score(res, req);
		});

	try {
		served::net::server server("0.0.0.0", "80", mux);
		server.run(10);
	}
	catch(const std::runtime_error &err) {
		std::clog << "Caught unhandled exception: " << err.what() << "\n";
		return (EXIT_FAILURE);
	}
	catch(...) {
		std::clog << "Caught unknown exception\n";
		return (EXIT_FAILURE);
	}

	return (EXIT_SUCCESS);
}
