# Requires recent Docker as it uses multi-stage build
# See https://docs.docker.com/engine/userguide/eng-image/multistage-build/

FROM cpprest-builder:3.11

COPY build-vol/Makefile /tmp
COPY build-vol/test.cpp /tmp
WORKDIR /tmp

RUN make -j4

FROM scratch
WORKDIR /
COPY --from=0 /tmp/test /prt-navy-srv

EXPOSE 80

ENTRYPOINT ["/prt-navy-srv"]
