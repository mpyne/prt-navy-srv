# Sample API -- Navy PRT score

This is an *extremely* simple API implementation server meant to create a
static Docker container that can be simply deployed behind an API gateway or
similar reverse proxy construct.

# Running

Running the container if you already have the image is simple enough. Assume
it's called `prt-navy-srv:3.11`, you could do something like:

    $ CONTAINER=$(docker run -d -P prt-navy-srv:3.11)
    $ docker port $CONTAINER
    80/tcp -> 0.0.0.0:32770
    $ curl http://localhost:32770/status
    {"status":"You'll find this battlestation is FULLY OPERATIONAL."}
    $ curl -d cardio_score=80 -d core_score=70        \
           -d upper_body_score=54 -d cardio_type=bike \
           http://localhost:32770/prt/getScore
    Method not allowed
    $ # oops, forgot to make it a GET instead of a POST, use -G
    $ curl -d cardio_score=80 -d core_score=70        \
           -d upper_body_score=54 -d cardio_type=bike \
           -G http://localhost:32770/prt/getScore
    {
    "score_cardio":66,
    "score_core":85,
    "score_upper_body":70,
    "score_readable":"GOOD MEDIUM",
    "score_overall":68
    }
    $ docker stop $CONTAINER

# Building

To build the container, it is sufficient to run `docker build` as normal (more
detail below).

You'll need the dependencies installed, including a separate container to act
as the build host.

## Dependencies

1. [Served](https://github.com/meltwater/served) C++ REST SDK.
2. [cpprest-builder](https://bitbucket.org/mpyne/cpprest-builder), which is the
   Docker container that would act as the C++ development environment /
   toolchain. It includes the first dependency (Served) already.

## Running docker build

With the `cpprest-builder` container available, you can build the actual app
server container image by using docker-build:

    $ cd prt-navy
    $ docker build -t prt-navy-srv:3.11 .

# Author

Michael Pyne <michael.pyne@gmail.com>. Do note that this is as much for
self-learning on how to use Docker as it is for a sample API so don't go
copying this blindly.
